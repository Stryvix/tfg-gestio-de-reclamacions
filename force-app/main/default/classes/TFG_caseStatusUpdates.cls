/********************************************************************************************
 * Name: TFG_CaseStatusUpdates
 * Copyright © 2021  Esteve Llaó    
 * ------------------------------------------------------------------------------------------
 * Proposito: Controlar los cambios de estado del case.
 * ------------------------------------------------------------------------------------------
 * Historial
 * -------
 * VERSION           AUTHOR             DATE                Description
 *     1.0           Esteve  Llaó       12/12/2021          Creacón     
********************************************************************************************/

public without sharing class TFG_caseStatusUpdates {

    public static void mandarNotificacionComercial(List<Case> listNewCase, List<Case> listOldCase) {

        Map<id,Case> mapaCasosNuevos= new Map<id,Case>();
        Map<Case,Case> mapaCaseNewOld = new Map<Case,Case>();
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        List<Messaging.SingleEmailMessage> messages = new List<Messaging.SingleEmailMessage>();
        Map<Id,Case> mapaEmailsGestores = new Map<Id,Case>();
        Map<Id,String> mapaCaseEmailGestor = new Map<Id,String>();
        List<Case> caosFiltrados = new List<Case>();
        EmailTemplate resultado = [SELECT Id, HtmlValue,Body, DeveloperName FROM EmailTemplate WHERE DeveloperName = 'TFG_RetencionLlamada' LIMIT 1];

        for (Case casoNuevo : listNewCAse) {
            mapaCasosNuevos.put(casoNuevo.id,casoNuevo);            
        }

        for (Case casoViejo : listOldCase){
            
            if (mapaCasosNuevos.containsKey(casoViejo.id) ) {
                if(mapaCasosNuevos.get(casoViejo.id).Subestados__c != casoViejo.Subestados__c && mapaCasosNuevos.get(casoViejo.id).Subestados__c == 'No conforme' ){
                    caosFiltrados.add(mapaCasosNuevos.get(casoViejo.id));
                }
            }
        }
        if(!caosFiltrados.isEmpty()){
            for (Case idsUserGestorCase : caosFiltrados) {
                mapaEmailsGestores.put(idsUserGestorCase.createdById, idsUserGestorCase);
            }
            
            List<User> gestores = [SELECT id,email FROM user WHERE id IN: mapaEmailsGestores.keySet()];
    
            for (User gestor : gestores) {
                if (mapaEmailsGestores.containsKey(gestor.id)) {
                    String subject = 'Retener cliente';
                    List<String> body = new List<String>();
            
                    body.add(resultado.HtmlValue);
                    String whoId = gestor.id;
                    String whatId = mapaEmailsGestores.get(gestor.id).id;
                    String emailBodyMessage = '';
                    List<Messaging.RenderEmailTemplateBodyResult> resList = Messaging.renderEmailTemplate(whoId, whatId, body);
        
                    if(resList.size() > 0){
                        emailBodyMessage = resList[0].getMergedBody();
                        emailBodyMessage = emailBodyMessage.replace('@@@@@', mapaEmailsGestores.get(gestor.id).caseNumber);
                        emailBodyMessage = emailBodyMessage.replace('#####', mapaEmailsGestores.get(gestor.id).id);
                    }
                    List<String> emails = new List<String>();
                    emails.add(gestor.email);
                    message.setToAddresses(emails);
                    message.setSubject(subject); 
                    message.setHtmlBody(emailBodyMessage);
                    messages.add(message);
                    
                }
            }
            Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
        }

    }

}