public without sharing class TFG_controlMilestoneExecution {
    public static void executeSecondaryMilestone(List<Case> listNewCase) {
        for (Case caso : listNewCase) {
            if (caso.IsEscalated == true){
                caso.TFG_triggerSecondMilestone__c = true;
            }
        }
    }
}