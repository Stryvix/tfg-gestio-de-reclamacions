/********************************************************************************************
 * Name: TFG_linkEntitlementToCase
 * Copyright © 2021  Esteve Llaó    
 * ------------------------------------------------------------------------------------------
 * Proposito: Vincular el entitlement process con el caso
 * ------------------------------------------------------------------------------------------
 * Historial
 * -------
 * VERSION           AUTHOR             DATE                Description
 *     1.0           Esteve  Llaó       12/12/2021          Creacón     
********************************************************************************************/

public without sharing class TFG_linkEntitlementToCase {
    public static void linkEntitlementToCase(List<Case> listNewCase) {
        if (Schema.sObjectType.Entitlement.isAccessible()) {
            
            List <Entitlement> entls = [select id, SlaProcess.name from Entitlement where SlaProcess.name = 'Resolucion de tickets' limit 1]; 
            System.debug('im in'+ entls);
            for (Case newCase : listNewCase) {
                if (!entls.isEmpty()) {
                    newCase.entitlementId = entls[0].Id;
                }
            }
        }
    }
}