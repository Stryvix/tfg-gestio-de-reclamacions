global class TFG_loadCaseTemplate implements Support.EmailTemplateSelector {
    //Constructor vacio
    global ID getDefaultEmailTemplateId(ID caseId) {
        // Select the case we're interested in, choosing any fields that are relevant to our decision
        id templateId;
        List<EmailTemplate> templates = [SELECT id FROM EmailTemplate WHERE DeveloperName = 'EmailDefaultBancoTE' LIMIT 1] ;
        if (!templates.isEmpty()) {
            EmailTemplate template = templates[0];    
            templateId = template.id;
        }
        return templateId;
    }
}