/********************************************************************************************
 * Name: TFG_Case_BU_TRHan
 * Copyright © 2021  Esteve Llaó    
 * ------------------------------------------------------------------------------------------
 * Proposito: Handler del trigger de case para controlar los cambios 
 * antes de una actualización en ese objecto.
 * ------------------------------------------------------------------------------------------
 * Historial
 * -------
 * VERSION           AUTHOR             DATE                Description
 *     1.0           Esteve  Llaó       12/12/2021          Creacón del trigger         
********************************************************************************************/

trigger TFG_Case_BU_TRhan on case (before update) {

    //TFG_controlMilestoneExecution.executeSecondaryMilestone(trigger.new);


}