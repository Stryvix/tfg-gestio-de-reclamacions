/********************************************************************************************
 * Name: TFG_Case_BI_TRHan
 * Copyright © 2021  Esteve Llaó    
 * ------------------------------------------------------------------------------------------
 * Proposito: Handler del trigger de case para controlar los cambios 
 * antes de crear un registro
 * ------------------------------------------------------------------------------------------
 * Historial
 * -------
 * VERSION           AUTHOR             DATE                Description
 *     1.0           Esteve  Llaó       12/12/2021          Creacón del trigger         
********************************************************************************************/


trigger TFG_Case_BI_TRhan on Case (before insert) {
    TFG_linkEntitlementToCase.linkEntitlementToCase(trigger.new);
}