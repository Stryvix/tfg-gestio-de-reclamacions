/********************************************************************************************
 * Name: TFG_Case_AU_TRHan
 * Copyright © 2021  Esteve Llaó    
 * ------------------------------------------------------------------------------------------
 * Proposito: Handler del trigger de case para controlar los cambios 
 * despues de una actualización en ese objecto.
 * ------------------------------------------------------------------------------------------
 * Historial
 * -------
 * VERSION           AUTHOR             DATE                Description
 *     1.0           Esteve  Llaó       12/12/2021          Creacón del trigger         
********************************************************************************************/

trigger TFG_Case_AU_TRhan on case (after update) {

    TFG_caseStatusUpdates.mandarNotificacionComercial(trigger.new, trigger.old);


}